SUMMARY = "cicada - unix shell"
HOMEPAGE = "https://github.com/mitnk/cicada/"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/mitnk/cicada.git;protocol=https;branch=master"
SRCREV = "11a158d78def29b05b0d24c891c21703af2cdfee"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=67604bb77815d1f793167aaf717dbb54"

INSANE_SKIP:${PN} += "already-stripped"
