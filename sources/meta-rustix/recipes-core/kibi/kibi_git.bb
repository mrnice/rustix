SUMMARY = "kibi - small text editor"
HOMEPAGE = "https://github.com/ilai-deutel/kibi"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/ilai-deutel/kibi.git;protocol=https;branch=master"
SRCREV = "f090efbed0aedd9d69bbb7fd03b0dbd0ec661f4e"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE-MIT;md5=9d44edff04fb50c55419d827f92aa869"
