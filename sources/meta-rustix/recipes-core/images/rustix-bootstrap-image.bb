# Rustix bootstrop image
# This image contains core-boot and is fully booting
# but uses busybox and so on :D
# This image is only ment to test rust and bring up rust replacements.

DESCRIPTION = "Rustix bootstrap image to test out replacements"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit core-image

IMAGE_INSTALL = "\
    packagegroup-core-boot \
    rustysd \
    nushell \
    ion \
    cicada \
    mgetty \
    minit \
    mingetty \
    util-linux \
"

EXTRA_IMAGE_FEATURES += "\
    dbg-pkgs \
    tools-debug \
    debug-tweaks \
    tools-sdk \
    dev-pkgs \
"

