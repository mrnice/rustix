# Rustix minimal image

DESCRIPTION = "Rustix minimal image"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit core-image

IMAGE_INSTALL = "\
    packagegroup-minimal-boot \
    uutils-coreutils \
    nushell \
    kibi \
    busd \
    sudo-rs \
    cicada \
"

#inherit extrausers
## tester01
#PASSWD = "\$5\$kBzimBs3pFIakhJv\$s5srm3sVQrJdtWCWgNSnon0r3DXlDn7c/olkBOx6iz0"
#EXTRA_USERS_PARAMS = "\
#    useradd -p '${PASSWD}' tester-jim; \
#    useradd -p '${PASSWD}' tester-sue; \
#    "
