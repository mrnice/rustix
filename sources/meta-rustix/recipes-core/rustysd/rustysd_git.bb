SUMMARY = "rustysd - systemd like service manager"
HOMEPAGE = "https://github.com/KillingSpark/rustysd"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "\
    git://github.com/KillingSpark/rustysd.git;protocol=https;branch=master \
    file://default.target \
    file://gettyAMA0.service \
    file://getty.target \
    file://local-filesystem.target \
    file://mount-proc.service \
    file://rustysd_config.toml \
"
SRC_URI:append:aarch64 = " file://0001-FIX-aarch64-char-is-unsignend.patch"
SRCREV = "aeb4140bbcae3cba54c87d773d3993e1a5b1eff4"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=13df02d6656dafde4ac24f5a0b25f86d"

# FIXME: consider shipping this files with other packages
#        but as the systemd recipe does the same keep it for now.
do_install:append() {
    install -d ${D}/usr/lib/rustysd/system
    install -m 0644 ${WORKDIR}/default.target ${D}/usr/lib/rustysd/system
    install -m 0644 ${WORKDIR}/gettyAMA0.service ${D}/usr/lib/rustysd/system
    install -m 0644 ${WORKDIR}/getty.target ${D}/usr/lib/rustysd/system
    install -m 0644 ${WORKDIR}/local-filesystem.target ${D}/usr/lib/rustysd/system
    install -m 0644 ${WORKDIR}/mount-proc.service ${D}/usr/lib/rustysd/system
    # config
    install -d ${D}/etc/rustysd
    install -m 0644 ${WORKDIR}/rustysd_config.toml ${D}/etc/rustysd
    # enable the targets
    install -d ${D}/etc/rustysd/system
    ln -sr ${D}/usr/lib/rustysd/system/default.target ${D}/etc/rustysd/system/default.target
    ln -sr ${D}/usr/lib/rustysd/system/gettyAMA0.service ${D}/etc/rustysd/system/gettyAMA0.service
    ln -sr ${D}/usr/lib/rustysd/system/getty.target ${D}/etc/rustysd/system/getty.target
    ln -sr ${D}/usr/lib/rustysd/system/local-filesystem.target ${D}/etc/rustysd/system/local-filesystem.target
    ln -sr ${D}/usr/lib/rustysd/system/mount-proc.service ${D}/etc/rustysd/system/mount-proc.service
}
