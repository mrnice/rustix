SUMMARY = "yash-rs - yash in rus"
HOMEPAGE = "https://github.com/magicant/yash-rs"
LICENSE = "GPLv3"

inherit features_check
REQUIRED_DISTRO_FEATURES += "usrmerge"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/magicant/yash-rs.git;protocol=https;branch=master"
SRCREV = "2eae979bcfd1fa4b5ac32e735789e7d18c7d34ec"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://README.md;md5=6955f1d5063b1362788846403b8ec8d6"

do_install:append() {
    ln -sr ${D}/usr/bin/yash ${D}/usr/bin/sh
}
