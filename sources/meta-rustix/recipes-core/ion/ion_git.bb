SUMMARY = "ion - redoxos shell"
HOMEPAGE = "https://github.com/redox-os/ion"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/redox-os/ion.git;protocol=https;branch=master"
SRCREV = "60bfb73351f0412c95b8ba2afe75e988514470a6"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6d00774c53660b1a93d4ebdf166646f9"
