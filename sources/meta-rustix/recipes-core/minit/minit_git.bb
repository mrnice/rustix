SUMMARY = "minit - minimal init"
HOMEPAGE = "https://github.com/mesalock-linux/minit"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/mesalock-linux/minit.git;protocol=https;branch=master \
    file://0001-remove-mount-mode-and-start-usr-bin-mgetty.patch \
"
SRCREV = "2f810e4a07b91ea56d18021a7a714c2df659fe7d"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=27da3feed21019ad7cd0b2f024ea2497"
