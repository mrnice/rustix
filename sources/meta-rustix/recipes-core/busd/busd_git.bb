SUMMARY = "busd - rust dbus broker"
HOMEPAGE = "https://github.com/dbus2/busd"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/dbus2/busd.git;protocol=https;branch=main"
SRCREV = "fb8b5caf0c5318927cc615e9b770b6a2bca2c6a8"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE-MIT;md5=b377b220f43d747efdec40d69fcaa69d"
