SUMMARY = "nushell - A new type of shell"
HOMEPAGE = "https://www.nushell.sh/"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

# FIXME: find out how to build nushell without openssl
DEPENDS += "openssl"

SRC_URI = "git://github.com/nushell/nushell.git;protocol=https;branch=main"
SRCREV="aeffa188f000a2e6a7d5adebf498bdb322425889"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0149a38d144ac342aee8408552100b3c"

