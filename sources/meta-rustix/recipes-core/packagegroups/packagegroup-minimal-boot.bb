SUMMARY = "Minimal boot requirements"
DESCRIPTION = "The minimal set of packages required to boot the system"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS:${PN} = "\
    base-files \
    base-passwd \
    rustysd \
    rustybox \
    minit \
    mgetty \
    yash-rs \
    ion \
"
