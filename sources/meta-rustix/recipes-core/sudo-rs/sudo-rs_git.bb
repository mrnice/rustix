SUMMARY = "sudo-rs - su and sudo written in rust"
HOMEPAGE = "https://github.com/memorysafety/sudo-rs"
LICENSE = "MIT"

DEPENDS = "libpam"
RDEPENDS:${PN} = "libpam"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/memorysafety/sudo-rs.git;protocol=https;branch=main"
SRCREV = "7f82d3a6528d8c47cc78e5ef7d94b9aefd763ffc"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE-MIT;md5=13daf9df88e199283af80bfa8324ce33"

# FIXME: find out how to pass options to cargo and don't strip the binary
INSANE_SKIP:${PN} += "already-stripped"
