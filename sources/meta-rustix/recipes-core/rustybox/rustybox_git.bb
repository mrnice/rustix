SUMMARY = "rustybox - something like busybox for rust"
HOMEPAGE = "https://gitlab.com/mrnice/rustybox"
LICENSE = "GPLv2"

inherit features_check
REQUIRED_DISTRO_FEATURES += "usrmerge"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://gitlab.com/mrnice/rustybox.git;protocol=https;branch=main"
SRCREV = "064deb2b09f112fd5b3fb40383a2ecb50e09174f"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://README.md;md5=f15b7bcc52765dbda6df53a2b9f1932e"
