SUMMARY = "uutils coreutils"
HOMEPAGE = "https://github.com/uutils/coreutils"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "git://github.com/uutils/coreutils.git;protocol=https;branch=main"
SRCREV = "356023b05520559dc3505811f6c882a7f05a8bb5"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e74349878141b240070458d414ab3b64"

do_install:append() {
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/b2sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/b3sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/base32
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/base64
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/basename
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/basenc
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/cat
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/cksum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/comm
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/cp
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/csplit
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/cut
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/date
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/dd
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/df
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/dir
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/dircolors
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/dirname
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/du
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/echo
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/env
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/expand
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/expr
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/factor
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/false
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/fmt
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/fold
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/hashsum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/head
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/join
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/link
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/ln
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/ls
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/md5sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/mkdir
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/mktemp
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/more
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/mv
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/nl
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/numfmt
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/od
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/paste
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/pr
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/printenv
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/printf
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/ptx
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/pwd
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/readlink
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/realpath
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/rm
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/rmdir
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/seq
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha1sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha224sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha256sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha3-224sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha3-256sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha3-384sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha3-512sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha384sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha3sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sha512sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/shake128sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/shake256sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/shred
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/shuf
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sleep
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sort
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/split
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/sum
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/tac
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/tail
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/tee
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/test
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/touch
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/tr
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/true
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/truncate
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/tsort
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/unexpand
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/uniq
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/unlink
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/vdir
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/wc
    ln -sr ${D}/usr/bin/coreutils ${D}/usr/bin/yes
}
