SUMMARY = "mgetty - minimal getty"
HOMEPAGE = "https://github.com/mesalock-linux/minit"
LICENSE = "MIT"

inherit cargo_bin
do_compile[network] = "1"

SRC_URI = "\
    git://github.com/mesalock-linux/mgetty.git;protocol=https;branch=master\
    file://0001-main.rs-use-usr-bin-ion.patch\
"
SRCREV = "f90b45706c8f2bd39f04def007698cba04b3a65b"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = "file://LICENSE;md5=27da3feed21019ad7cd0b2f024ea2497"
