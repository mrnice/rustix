# Rustix

Rustix is my playground to see how far we can get with rust only Linux distribution. There are so many replacements for coreutils and Linux tools in general that I started to wonder if it is possible to use only rust based tools.

## Project goal

The main project goal is to have fun ;) so don't take this to serious. The second goal is to use only rust based tools.

The exeptions are:

* Build system is based on openembedded
* Kernel is Linux therefore mostly C
* Bootloader
* Shell scripts
* Config files

All other tools including pid 1 should be rust based.

## Target systems

As this is openembedded based it should be possible to use this layer for quite all machines for which the rust compiler is able to create code for but for simplicity I will concentrate on qemu aarch64.

## Graphical interface

Currently out of scope. But there is at least one [wayland implementation in rust](https://github.com/Smithay/wayland-rs) I'm aware of.

## Contribution

Feel free to contribute :)

## Roadmap

Current milestone is to get the thing to boot and explore the possibilities. A POSIX shell would be nice but I will might start with nushell first.

## License
The layer is MIT licensed with mixed licenses for all the components.

